# Block11

Для сборки образа
```
docker build . -f Dockerfile-nginx -t block11-nginx
```
Для загрузки локального докер-образа в minikibe
```
minikube image load block11-nginx
```
Создание деплоймента в кубере
```
kubectl apply -f deployment.yaml
```
Создание сервиса в кубере
```
kubectl apply -f service.yaml
```
Перенаправление портов на локальную машину для доступа к странице
```
kubectl port-forward nginx-service 80:80
```
